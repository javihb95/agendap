
package Data;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Movimiento {
@XmlElement
    private Integer id;
@XmlElement
private String idGrupo;
@XmlElement
private String entradaSalida;
@XmlElement
private Date fechahora;
@XmlElement
private Persona idPersona; 

public Movimiento(Integer id, String idGrupo, String entradaSalida, Date fechahora, Persona idPersona) {
        this.id = id;
        this.idGrupo = idGrupo;
        this.entradaSalida = entradaSalida;
        this.fechahora = fechahora;
        this.idPersona = idPersona;
    }

    public Movimiento() {
    }

    public Integer getId() {
        return id;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public String getEntradaSalida() {
        return entradaSalida;
    }

    public Date getFechahora() {
        return fechahora;
    }

    public Persona getIdPersona() {
        return idPersona;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }

    public void setEntradaSalida(String entradaSalida) {
        this.entradaSalida = entradaSalida;
    }

    public void setFechahora(Date fechahora) {
        this.fechahora = fechahora;
    }

    public void setIdPersona(Persona idPersona) {
        this.idPersona = idPersona;
    }


}
