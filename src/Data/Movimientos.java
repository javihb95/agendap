
package Data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Movimientos {
    @XmlElement(name = "movimiento")
    // Lista de donde depende toda el tableview
    private List<Movimiento> listaMovimiento;

    public Movimientos() {
        listaMovimiento = new ArrayList();
    }
    
    public List<Movimiento> getlistaMovimiento() {
        return listaMovimiento;
    }

    public void setlistaMovimiento(List<Movimiento> listaPersona) {
        this.listaMovimiento = listaPersona;
    }
}

