
package Data;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



/**
 *
 * @author JavierHolgado <javier.holgado>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Persona {
    @XmlElement
    private int id;
    @XmlElement
    private String nombre;
    @XmlElement
    private String apellidos;
    @XmlElement
    private String sexo;
    @XmlElement
    private String dni; 
    @XmlElement
    private String direccion;
    @XmlElement
    private String codigoP;
    @XmlElement
    private String localidad;
    @XmlElement
    private String telefono;
    @XmlElement
    private String movil;
    @XmlElement
    private Date fecha;
    
    //Clase Persona, propiedades getters y setters 
    public Persona(String nombre, String Apellidos, String Sexo, String Dni,
            String Direccion, String codigoP, String localidad, String telefono,
            String movil, Date fecha) {
        this.nombre = nombre;
        this.apellidos = Apellidos;
        this.sexo = Sexo;
        this.dni = Dni;
        this.direccion = Direccion;
        this.codigoP = codigoP;
        this.localidad = localidad;
        this.telefono = telefono;
        this.movil = movil;
        this.fecha = fecha;
    }
    public Persona(){}

    public int getId() {
        return id;
    }
    public void setid(int id){
        this.id = id;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setApellidos(String Apellidos) {
        this.apellidos = Apellidos;
    }
    
    public void setSexo(String Sexo) {
        this.sexo = Sexo;
    }
    
    public void setDni(String Dni) {
        this.dni = Dni;
    }
    
    public void setDireccion(String Direccion) {
        this.direccion = Direccion;
    }

    
    public void setCodigoP(String codigoP) {
        this.codigoP = codigoP;
    }
    
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
 
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }
   
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public String getDni() {
        return dni;
    }

    public String getDireccion() {
        return direccion;
    }



    public String getCodigoP() {
        return codigoP;
    }

    public String getLocalidad() {
        return localidad;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getMovil() {
        return movil;
    }

    public Date getFecha() {
        return fecha;
    }
    
}
