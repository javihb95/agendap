
package Data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Personas {
    
    @XmlElement(name = "persona")
    // Lista de donde depende toda el tableview
    private List<Persona> listaPersona;

    public Personas() {
        listaPersona = new ArrayList();
    }
    
    public List<Persona> getlistaPersona() {
        return listaPersona;
    }

    public void setlistaPersona(List<Persona> listaPersona) {
        this.listaPersona= listaPersona;
    }
}
