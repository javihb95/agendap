
package FXML;

import agendap.AgendaP;
import Data.Persona;
import Data.Personas;
import agendap.Util;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author JavierHolgado <javier.holgado>
 */
public class FXMLController implements Initializable {
    @FXML
    private  TextField jTextfieldNombre;
    @FXML
    private  TextField jTextfieldApellidos;
    @FXML
    private  TextField jTextfieldSexo;
    @FXML
    private  TextField jTextfieldDni;
    @FXML
    private  TextField jTextfieldDireccion;
    @FXML
    private  TextField jTextfieldCodigoP;
    @FXML
    private  TextField jTextfieldlocalidad;
    @FXML
    private  TextField jTextfieldtelefono;
    @FXML
    private  TextField jTextfieldmovil;
    @FXML
    private  Button ButtonConfirmar;
    @FXML
    private  Button ButtonEditar;
    @FXML
    private Label labelTitulo;
    @FXML
    private DatePicker DatePickerFecha;
    
    private  Persona persona;
    
    private boolean newP = false;
    
    private TableView<Persona> table;
    
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
         
    }    
    
    public void setPersona(Persona persona){
        this.persona = persona;
        System.out.println(persona.getNombre());
        jTextfieldNombre.setText(persona.getNombre());
        jTextfieldApellidos.setText(persona.getApellidos());
        jTextfieldSexo.setText(persona.getSexo());
        jTextfieldDni.setText(persona.getDni());
        jTextfieldDireccion.setText(persona.getDireccion());
        jTextfieldCodigoP.setText(persona.getCodigoP());
        jTextfieldlocalidad.setText(persona.getLocalidad());
        jTextfieldtelefono.setText(persona.getTelefono());
        jTextfieldmovil.setText(persona.getMovil());
        Util.setDateInDatePicker(DatePickerFecha, persona.getFecha());
    }
    public void newPersona(){
        labelTitulo.setText("Nuevo");
        newP = true;
    }
        
    @FXML
    private void onMouseClickedConfirm(MouseEvent event) {
        // Metodo que crea o edita una persona dependiendo de la variable newP, si es falsa los textfield
        // recogen todos los datos del objeto y la posicion del mismo. 
        if(newP == false){
            persona.setNombre(jTextfieldNombre.getText());
            persona.setApellidos(jTextfieldApellidos.getText());
            persona.setSexo(jTextfieldSexo.getText());
            persona.setDni(jTextfieldDni.getText());
            persona.setDireccion(jTextfieldDireccion.getText());
            persona.setCodigoP(jTextfieldCodigoP.getText());
            persona.setLocalidad(jTextfieldlocalidad.getText());
            persona.setTelefono(jTextfieldtelefono.getText());
            persona.setMovil(jTextfieldmovil.getText());
            persona.setFecha(Util.getDateFromDatePicket(DatePickerFecha));
            //ViewTable.data.set(ViewTable.selectedPerson, persona);
            int selectedPerson = table.getSelectionModel().getSelectedIndex();
            if(this.datoscorrectos(persona) == true){
                table.getItems().set(selectedPerson, persona);
                Personas listaPUT = new Personas();
                listaPUT.getlistaPersona().add(persona);
                Util.connectToServer("PUT", listaPUT); 
                int lastScreensNumber = AgendaP.root.getChildren().size() - 1;
                AgendaP.root.getChildren().remove(lastScreensNumber);
            }
        }else{
        // Si es verdadera crea una nueva persona donde guarda los datos introducidos
        // y lo añade a la tabla
            persona = new Persona(jTextfieldNombre.getText(), 
                   jTextfieldApellidos.getText(), jTextfieldSexo.getText(), 
                   jTextfieldDni.getText(), jTextfieldDireccion.getText(),
                   jTextfieldCodigoP.getText(), jTextfieldlocalidad.getText(), 
                   jTextfieldtelefono.getText(), jTextfieldmovil.getText(),
                   Util.getDateFromDatePicket(DatePickerFecha));
            if(this.datoscorrectos(persona) == true){
                table.getItems().add(persona);
                int indice = table.getItems().indexOf(persona);
        // Creo otra lista Personas donde se guardara los datos que pasaremos 
        // a XML.
                Personas listaAux = new Personas();
                listaAux.getlistaPersona().add(persona);
        //JavaToXml.javatoxml(listaAux);
                Util.connectToServer("POST", listaAux);
        // Dspues de añadir la persona a la base de datos recibo su id y se lo añado
                table.getItems().get(indice).setid(Util.getPersonaAux().getId());
        // Volvemos a la "ventana" anterior eliminando la ultima añadida.
                int lastScreensNumber = AgendaP.root.getChildren().size() - 1;
                AgendaP.root.getChildren().remove(lastScreensNumber);
            }
        }
    }
    public void setTable(TableView<Persona> table){
        this.table= table;
    }
    @FXML
    private void onMouseClickedBack(MouseEvent event) {
        // Volvemos a la "ventana" anterior eliminando la ultima añadida.    
        int lastScreensNumber = AgendaP.root.getChildren().size() - 1;
        AgendaP.root.getChildren().remove(lastScreensNumber);  
    }

    @FXML
    void volverAPrincipal(ActionEvent event) {
        int lastScreensNumber = AgendaP.root.getChildren().size() - 1;
        AgendaP.root.getChildren().remove(lastScreensNumber);     
    }
    private  boolean datoscorrectos(Persona persona){
        boolean result = false;
        System.out.println(" COdigo " + persona.getCodigoP().length() + " Dni " + persona.getDni().length() +
            " Movil " +  persona.getMovil().length() + " Telefono " + persona.getTelefono().length());
        System.out.println(Util.getDateFromDatePicket(DatePickerFecha));
        if(persona.getCodigoP().length() == 5 && persona.getDni().length() == 9 
                && persona.getMovil().length() == 9 && persona.getTelefono().length() == 9 
                    && Util.getDateFromDatePicket(DatePickerFecha) != null)
        {
            if(persona.getSexo().equals("H") || persona.getSexo().equals("M")){
                result = true; 
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error ");
            alert.setHeaderText(null);
            alert.setContentText("los datos introducidos son erroneos");
            alert.showAndWait();
        }
        return result;
    }
}
