/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FXML2;

import Data.Movimiento;
import Data.Movimientos;
import Data.Persona;
import Data.Personas;
import agendap.AgendaP;
import agendap.Util;
import agendap.ViewTable;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * FXML Controller class
 *
 * @author JavierHolgado <javier.holgado>
 */
public class FXMLMovimientoController implements Initializable {

    @FXML
    private TableView<Movimiento> tablemov;
    @FXML
    private Button volver;
    private ObservableList<Movimiento> data;
    @FXML
    private Button delete;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    
    }
    public  void setTable(){
               
        tablemov.setEditable(true);
        tablemov.setItems(data);

        TableColumn idcol = new TableColumn("Id");
        idcol.setMinWidth(75);
        idcol.setCellValueFactory(
                new PropertyValueFactory<Movimiento, Integer>("Id"));

        TableColumn idGrupocol = new TableColumn("IdGrupo");
        idGrupocol.setMinWidth(75);
        idGrupocol.setCellValueFactory(
                new PropertyValueFactory<Movimiento, Integer>("IdGrupo"));


        TableColumn colES = new TableColumn("EntradaSalida");
        colES.setMinWidth(75);
        colES.setCellValueFactory(
                new PropertyValueFactory<Movimiento, String>("EntradaSalida"));

        TableColumn colFecha = new TableColumn("Fechahora");
        colFecha.setMinWidth(80);
        colFecha.setCellValueFactory(
                new PropertyValueFactory<Movimiento, Date>("Fechahora"));
        
        this.setDateFormatColumn(colFecha, "dd/MM/yy" );
        tablemov.getColumns().addAll(idcol,idGrupocol,colES,colFecha);
    }
    
    public void getmovimiento(Persona persona){
        Movimientos mov = new Movimientos();
         
        try {
            Personas listaAux = new Personas();
            listaAux.getlistaPersona().add(persona);
            Logger.getLogger(FXMLMovimientoController.class.getName()).log(Level.INFO, 
                    String.valueOf(listaAux.getlistaPersona().get(0).getId())
                            + "es el id de la persona en la lista a pasar");
            JAXBContext jaxbContext = JAXBContext.newInstance(Movimientos.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            InputStreamReader isr = Util.connectToServerMov("GET", null,listaAux);
            Object response = jaxbUnmarshaller.unmarshal(isr);
            mov = (Movimientos)response;
            if(mov.getlistaMovimiento().size() == 0){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error ");
                alert.setHeaderText(null);
                alert.setContentText("Esta persona no tiene movimientos");
                alert.showAndWait();
            }else{
                data = FXCollections.observableArrayList(mov.getlistaMovimiento());
            }
        }catch(JAXBException ex) {
            Logger.getLogger(FXMLMovimientoController.class.getName()).log(Level.SEVERE, null, ex);   
        }
    }
    
    @FXML
    void volverAPrincipal(ActionEvent event) {
        int lastScreensNumber = AgendaP.root.getChildren().size() - 1;
        AgendaP.root.getChildren().remove(lastScreensNumber);     
    }

    @FXML
    private void deletemov(ActionEvent event) {
        Personas listaAux = new Personas(); 
        listaAux.getlistaPersona().add(ViewTable.personaselec());
        Movimientos mov = new Movimientos();
        mov.getlistaMovimiento().add(tablemov.getSelectionModel().getSelectedItem());
        Util.connectToServerMov("DELETE", mov, listaAux);
        
    }
    public  void setDateFormatColumn(TableColumn dateColumn, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        dateColumn.setCellFactory(myDateTableCell -> {
            return new TableCell<Object, Date>() {
                @Override
                protected void updateItem(Date date, boolean dateIsEmpty) {
                    super.updateItem(date, dateIsEmpty);
                    if (date == null || dateIsEmpty) {
                        setText(null);
                    } else {
                        setText(simpleDateFormat.format(date));
                    }
                }
            };
        });
    }
}
