
package agendap;

import Data.Personas;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author 1DAW06
 */
public class AgendaP extends Application {
   
    public static StackPane root = new StackPane();
    private Personas listapersona = new Personas();
    @Override
    public void start(Stage primaryStage) {
//      Conexion con el servidor mediante proxy (en lineas de comando)  
//      System.setProperty("http.proxyHost", "127.0.0.1");
//      System.setProperty("http.proxyPort", "8888");
        listapersona = (Personas)XmltoJava.xmlToJava(Util.connectToServer("GET",null));
        ViewTable viewtable = new ViewTable(listapersona);
        root.getChildren().add(viewtable);
        Scene scene = new Scene(root, 400, 500);
        primaryStage.setTitle("Agenda");
        primaryStage.setScene(scene);
        primaryStage.show();
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
