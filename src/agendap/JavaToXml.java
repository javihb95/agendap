
package agendap;
import Data.Personas;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
public class JavaToXml {
    private static final Logger LOG = Logger.getLogger(JavaToXml.class.getName());
    
    public static void javatoxml(Personas listaAux){
        //Pasa la lista que recibe a xml en un archivo  
        try {
            JAXBContext context = JAXBContext.newInstance(Personas.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(listaAux, System.out);
            BufferedWriter bw = new BufferedWriter(new FileWriter("ListaPersona.xml"));
            marshaller.marshal(listaAux, bw);
        } catch (JAXBException e) {
            LOG.getLogger(JavaToXml.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException ex) {
            Logger.getLogger(JavaToXml.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
