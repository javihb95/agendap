
package agendap;

import Data.Persona;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 1DAW06
 */
public class Lecturas {
    private static  ArrayList<Persona> arrayPersonas = new ArrayList();
    private static final Logger LOG = Logger.getLogger(Lecturas.class.getName());
    private static boolean Error = false;
    
    public static boolean getErrors(){
        return Error; 
    }
    
    public static ArrayList<Persona> getArrayPersonas() {
        return arrayPersonas;
    }
    
    public static void getpersonasCsv(){ 
    BufferedReader br = null;
    String nombreFichero = "agenda.csv";     
        try {
           //Crear un objeto BufferedReader al que se le pasa 
           //   un objeto FileReader con el nombre del fichero
           br = new BufferedReader(new FileReader(nombreFichero));
           //Leer la primera línea, guardando en un String
           String texto = br.readLine();
           SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
           formatter.setLenient(false);
           //Repetir mientras no se llegue al final del fichero
           while(texto != null)
           {
               String[] list = texto.split(";");
               Date fecha = formatter.parse(list[9]);
               //Hacer lo que sea con la línea leída
               arrayPersonas.add(new Persona(list[0],list[1],list[2],list[3],list[4],list[5],list[6],
               list[7],list[8],fecha));
               //Leer la siguiente línea
               texto = br.readLine();
           }
        
        }
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(IOException e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        catch(ArrayIndexOutOfBoundsException | ParseException e){
            LOG.log(Level.FINE,e.getMessage());
            arrayPersonas = null;
            Error = true;
            
        }
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
    }
}
