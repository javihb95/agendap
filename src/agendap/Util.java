
package agendap;

import Data.Movimientos;
import Data.Persona;
import Data.Personas;
import FXML.FXMLController;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
public class Util {
    private static Persona  personaAux = new Persona();
    private static Personas listapersonaAux = new Personas();
    private static final Logger LOG = Logger.getLogger(Util.class.getName());
    
    public static Personas getListapersonaAux() {
        return listapersonaAux;
    }
    
    public static Persona getPersonaAux() {
        return personaAux;
    }
    
    public static void setDateFormatColumn(TableColumn dateColumn, String dateFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        dateColumn.setCellFactory(myDateTableCell -> {
            return new TableCell<Object, Date>() {
                @Override
                protected void updateItem(Date date, boolean dateIsEmpty) {
                    super.updateItem(date, dateIsEmpty);
                    if (date == null || dateIsEmpty) {
                        setText(null);
                    } else {
                        setText(simpleDateFormat.format(date));
                    }
                }
            };
        });
    }
    public static String datetoString(Date date){
        DateFormat df = new SimpleDateFormat("dd/MM/yy");
        String newDate = df.format(date);
     return newDate;
    }
    public static Date stringtoDate(String string){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
        Date fecha = null;
        try {
            fecha = formatter.parse(string);
        } catch (ParseException ex) {
        Logger.getLogger(FXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fecha;
    }
   
    
    public static InputStreamReader connectToServer(String peticion,Personas listaAux){
        InputStreamReader isr = null;
        try {
            //Lecturas.showReads();
            // XmltoJava.xmltoJavaUrl();
            String strConnection = "http://192.168.12.205:8084/WebAgenda/NewServlet";
            URL url = new URL(strConnection);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");
            
            // Se va a realizar una petición que le paso por parametro
            conn.setRequestMethod(peticion);
            // Declaraciones de los convertidores a xml que usaremos en las peticiones 
            JAXBContext context = JAXBContext.newInstance(Personas.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            
            switch(peticion) {
                case "GET":
                // Alamcena todos los datos que envia el servidor
                // en la llamada se almacena en el Tableview por el return 
                    isr = new InputStreamReader(conn.getInputStream());   
                    break;
                case "POST":
                // Pasa el objeto a xml y lo manda al servidor
                    marshaller.marshal(listaAux,conn.getOutputStream());
                    Logger.getLogger(Util.class.getName()).log(Level.INFO, 
                          "Nombre de la persona en el cliente " 
                                  + listaAux.getlistaPersona().get(0).getSexo());
                // Ejecutar la conexión y obtener la respuesta para poder poner el
                // id en el objeto que esta en el cliente 
                    isr = new InputStreamReader(conn.getInputStream());
                    Personas personas = (Personas) XmltoJava.xmlToJava(isr);
                    for (Persona persona : personas.getlistaPersona()){
                        LOG.log(Level.INFO, String.valueOf(persona.getId()));
                        personaAux = persona;
                        listapersonaAux.getlistaPersona().add(personaAux);
                    }
                break;
                case "DELETE":
                // Paso la persona a eliminar y mando a la base de datos     
                    LOG.log(Level.INFO, "He entrado en el caso delete");
                    marshaller.marshal(listaAux,conn.getOutputStream());
                    isr = new InputStreamReader(conn.getInputStream());
                    LOG.log(Level.INFO, "He mandado la persona a eliminar");
                // La respuesta sera un mensaje por si ha borrado la persona correctamente    
                    InputStreamReader respuesta = new InputStreamReader(conn.getInputStream());
                    String mensaje =  Util.xmlDOM(respuesta);
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("INFO ");
                        alert.setHeaderText(null);
                        alert.setContentText(mensaje);
                        alert.showAndWait();
                    
                    break;
                case "PUT":
                // Paso los nuevos datos de la persona a XML y los amndo a la base de datos     
                    marshaller.marshal(listaAux,conn.getOutputStream());
                    isr = new InputStreamReader(conn.getInputStream());
                    // La respuesta sera un mensaje por si ha borrado la persona correctamente    
                    InputStreamReader respuestaEditar = new InputStreamReader(conn.getInputStream());
                    String mensajeEditar =  Util.xmlDOM(respuestaEditar);
                    Alert alertEditar = new Alert(Alert.AlertType.INFORMATION);
                        alertEditar.setTitle("INFO ");
                        alertEditar.setHeaderText(null);
                        alertEditar.setContentText(mensajeEditar);
                        alertEditar.showAndWait();
                break;
            }
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(AgendaP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AgendaP.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return isr;
    }
    public static InputStreamReader connectToServerMov(String peticion, Movimientos listamov, Personas listaAux){
    InputStreamReader isr = null;
        try {
            Logger.getLogger(Util.class.getName()).log(Level.INFO, String.valueOf
                        (listaAux.getlistaPersona().get(0).getId()));
            String strConnection = "http://192.168.12.205:8084/WebAgenda/ServletMovimiento"
                    + "?id=" + listaAux.getlistaPersona().get(0).getId();
            URL url = new URL(strConnection);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");
            conn.setRequestMethod(peticion);
            
            JAXBContext context = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            Marshaller marshaller = context.createMarshaller();
            switch(peticion) {
                case "GET":
                    // mando la persona seleccionada
                    //marshaller.marshal(listaAux, conn.getOutputStream());
                    isr = new InputStreamReader(conn.getInputStream());   
                    break;
                case "DELETE": 
                    marshaller.marshal(listamov,conn.getOutputStream());
                    isr = new InputStreamReader(conn.getInputStream());
            }
            
        
        } catch (MalformedURLException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JAXBException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    return isr;
    }
    public static String xmlDOM(InputStreamReader respuesta){
        String mensaje = null;
        try {
        // Pasar el InputStreamReader a BufferedReader, y este a InputSource
            BufferedReader xml = new BufferedReader(respuesta);
            InputSource input = new InputSource(xml);
            DocumentBuilderFactory personasCreadorDocumento = DocumentBuilderFactory.newInstance();
            DocumentBuilder creadorDocumento = personasCreadorDocumento.newDocumentBuilder();
            Document documento = creadorDocumento.parse(input);
            NodeList lista = documento.getElementsByTagName("mensaje");
            Node empleado = lista.item(0);
            mensaje = empleado.getTextContent();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mensaje;
    } 
    public static void setDateInDatePicker(DatePicker datePicker, Date date) {
        if(date != null) {
            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(); 
            datePicker.setValue(localDate);
        }
    }
    public static Date getDateFromDatePicket(DatePicker datePicker) {
        LocalDate localDate = datePicker.getValue();
        if(localDate != null) {
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date date = Date.from(instant);  
            return date;
        } else {
            return null;
        }
    }
     
}
