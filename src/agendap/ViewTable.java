
package agendap;

import Data.Personas;
import Data.Persona;
import FXML.FXMLController;
import FXML2.FXMLMovimientoController;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JavierHolgado <Javier.holgado>
 */
public class ViewTable extends VBox{
    private static TableView<Persona> table = new TableView<Persona>();
    private HBox hbox = new HBox();
    private Button nuevo = new Button("Nuevo");
    private Button editar = new Button("Editar");
    private Button eliminar = new Button("Eliminar");
    private Button movimientos = new Button("movimientos");
    private Button importarCsv = new Button("importarCsv");
    private ObservableList<Persona> data;

   
            
    public ViewTable(Personas listapersona){
        this.setStyle("-fx-padding: 10; -fx-spacing: 10;");
        if(Lecturas.getErrors() == true){
            showErrosMessage();
        }else{
            //listapersona.setlistaPersona(Lecturas.getArrayPersonas());
            data = FXCollections.observableArrayList(listapersona.getlistaPersona());
        }
        table.setEditable(true);
        
        table.setMaxSize(375, 600);
        table.setItems(data);

        TableColumn idcol = new TableColumn("id");
        idcol.setMinWidth(50);
        idcol.setCellValueFactory(
                new PropertyValueFactory<Persona, Integer>("id"));

        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setMinWidth(150);
        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Persona, String>("nombre"));    
        
        TableColumn DniCol = new TableColumn("Dni");
        DniCol.setMinWidth(50);
        DniCol.setCellValueFactory(
                new PropertyValueFactory<Persona, String>("dni"));   
        
        TableColumn fechaCol = new TableColumn("Fecha");
        fechaCol.setMinWidth(80);
        fechaCol.setCellValueFactory(
                new PropertyValueFactory<Persona, Date>("fecha"));

        Util.setDateFormatColumn(fechaCol, "dd/MM/yy");

        table.getColumns().addAll(idcol,firstNameCol,DniCol,fechaCol);
        hbox.setStyle("-fx-spacing: 10;");
        hbox.getChildren().addAll(nuevo,editar, eliminar,movimientos,importarCsv);
        this.getChildren().addAll(hbox,table);
        
        eliminar.setOnAction(new EventHandler<ActionEvent>() {            
        @Override
        public void handle(ActionEvent event) {
            Persona persona = table.getSelectionModel().getSelectedItem();
            Personas listaAux = new Personas();
            listaAux.getlistaPersona().add(persona);
            listapersona.getlistaPersona().remove(persona);
            Util.connectToServer("DELETE", listaAux);
            data.remove(persona);
            //JavaToXml.javatoxml(listaAux);
            //Lecturas.getArrayPersonas().remove(persona);
        }
        });
    
        editar.setOnAction(new EventHandler<ActionEvent>() {            
        @Override
        public void handle(ActionEvent event) {  
            Persona persona = table.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/FXML/FXML.fxml"));
                AgendaP.root.getChildren().add(fXMLLoader.load());
                FXMLController controller = fXMLLoader.getController();
                controller.setPersona(persona);
                controller.setTable(table);
                
            } catch (IOException ex) {
                Logger.getLogger(ViewTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        });
    nuevo.setOnAction(new EventHandler<ActionEvent>() {            
        @Override
        public void handle(ActionEvent event) { 
            try {
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/FXML/FXML.fxml"));
                AgendaP.root.getChildren().add(fXMLLoader.load());
                FXMLController controller = (FXMLController)fXMLLoader.getController();
                controller.newPersona();
                controller.setTable(table);
                // listapersona.getlistaPersona().add(controller.getPersona());
            } catch (IOException ex) {
                Logger.getLogger(ViewTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    });
    movimientos.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            try {
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("/FXML2/FXMLMovimiento.fxml"));
                AgendaP.root.getChildren().add(fXMLLoader.load());
                FXMLMovimientoController controller = (FXMLMovimientoController)fXMLLoader.getController();
                Persona persona = table.getSelectionModel().getSelectedItem();
                Logger.getLogger(ViewTable.class.getName()).log(Level.INFO, String.valueOf(persona.getId()
                        + "Es el id de la persona seleccionada en el viewtable"));
                controller.getmovimiento(persona);
                controller.setTable();
            } catch (IOException ex) {
                Logger.getLogger(ViewTable.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    });
    importarCsv.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            Lecturas.getpersonasCsv();
            Personas listaPersonas = new Personas();
            listaPersonas.getlistaPersona().addAll(Lecturas.getArrayPersonas());
            Util.connectToServer("POST", listaPersonas);
            //table.setItems(FXCollections.observableArrayList(Util.getListapersonaAux().getlistaPersona()));
            data.addAll(Util.getListapersonaAux().getlistaPersona());
        }   
    });
    };
    public static Persona personaselec(){
        Persona personaSeleccionada =  table.getSelectionModel().getSelectedItem();
    return personaSeleccionada;
    }
    private static void showErrosMessage(){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle("Error ");
        alert.setHeaderText(null);
        alert.setContentText("La lectura se ha abortado; los datos introducidos son erroneos");
        alert.showAndWait();
    } 
    
}
