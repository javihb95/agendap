
package agendap;

import Data.Persona;
import Data.Personas;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author JavierHolgado <javier.holgado>
 */
public class XmltoJava {
    public static void xmltoJava(){
        
        try {
            BufferedReader br = new BufferedReader(new FileReader("ListaPersona.xml"));
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Personas ListaPersona = (Personas) jaxbUnmarshaller.unmarshal(br);
            for(Persona persona : ListaPersona.getlistaPersona()) {
                System.out.println("id: " + persona.getId());
                System.out.println("nombre: " + persona.getNombre());
                System.out.println("apellidos: " + persona.getApellidos());
                System.out.println("fecha: " + Util.datetoString(persona.getFecha()));
                System.out.println();
            }
        } catch (FileNotFoundException | JAXBException ex) {
            Logger.getLogger(XmltoJava.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void xmltoJavaUrl(){
        System.out.println("ENTRO EN EL METODO");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            URL url = new URL("http://javierholgado.esy.es/Lista.xml");
            InputStream is = url.openStream();
            Personas personasUrl = (Personas) jaxbUnmarshaller.unmarshal(is);
            System.out.println("LINEA SEIS");
            System.out.println(personasUrl.getlistaPersona().size());
        for(Persona persona : personasUrl.getlistaPersona()) {
            System.out.println("LINEA SIETE");
            System.out.println(personasUrl.getlistaPersona().size());
            System.out.println("id: " + persona.getId());
            System.out.println("nombre: " + persona.getNombre());
            System.out.println("apellidos: " + persona.getApellidos());
            System.out.println("fecha: " + Util.datetoString(persona.getFecha()));
            System.out.println();
        }    
        } catch (Exception ex) {
            Logger.getLogger(XmltoJava.class.getName()).log(Level.SEVERE, null, ex);
        }     
    }
    public static Object xmlToJava( InputStreamReader isr ){
        Object response = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            response = jaxbUnmarshaller.unmarshal(isr); 
            
                    } catch (JAXBException ex) {
            Logger.getLogger(XmltoJava.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}
